const port = process.env.NODE_CLENT_PORT;

module.exports = {
  apps: [
    {
      name: "API",
      instances: 1,
      autorestart: true,
      watch: false,
      max_memory_restart: "1G",
      cwd: "",
      script: "./node_modules/nuxt/bin/nuxt.js",
      args: `--port=${port}`,
      env: {
        NODE_ENV: "development"
      },
      env_production: {
        NODE_ENV: "production"
      }
    }
  ]
};
